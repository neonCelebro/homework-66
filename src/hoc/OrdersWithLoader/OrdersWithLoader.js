import React, {Component, Fragment} from 'react';
import Spinner from '../../UI/Spinner/Spinner';

const OrdersWithLoader = (WrappedComponent, axios) => {
  return class OrdersWithLoader extends Component {
    state = {
      isLoading: false
    };
    componentDidCatch = (error, info) => {
      this.setState({hasError: true, errorMessage: error});
    };

    componentDidMount() {
      axios.interceptors.request.use((req) => {
        this.setState({isLoading: true});
        return req;
      });

      axios.interceptors.response.use((res) => {
        this.setState({isLoading: false});
        return res;
      });
    }
    render() {
      return (<Fragment>
        <Spinner hide={this.state.isLoading
            ? 'Spinner'
            : 'hide'}/>
        <WrappedComponent {...this.props}/>
        }
      </Fragment>);
    }
  }
};

export default OrdersWithLoader;
