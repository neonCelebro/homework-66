import React, {Component} from 'react';
import axiosError from '../axios';

class ErrorBoundary extends Component {
  state = {
    hasError: false,
  };

  componentDidCatch = (error, info) => {
    axiosError.post('/errors.json', error).then(
    )
  };

  render () {
    if (this.state.hasError) {
      return <div>Something wrong happened</div>;
    } else {
      return this.props.children;
    }
  }
}

export default ErrorBoundary;
