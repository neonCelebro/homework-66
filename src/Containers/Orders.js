import React, { Component } from 'react';
import OrdersWithLoader from "../hoc/OrdersWithLoader/OrdersWithLoader";
import Form from '../UI/Form/Form';
import Table from '../Components/Table/Table';
import axios from '../axios';

class Orders extends Component {
  state = {
        name: '',
        quantity: '',
        price: '',
      items: [],
  };
  getName = (e) => this.setState({name: e.target.value});
  getQuantity = (e) => this.setState({quantity: e.target.value});
  getPrice = (e) => this.setState({price: e.target.value});

  setInfoInState = () =>{
      axios.get('items.json').then((response) => this.setState({items: response.data}));
  };

  addNewItem = (e) =>{
    e.preventDefault();
    const items = [...this.state.items];
    const newItem = {
      name: this.state.name,
      quantity: this.state.quantity,
      price: this.state.price,
    };
    items.push(newItem);
    axios.put('/items.json', items)
    .then(this.setState({items}))
    .then(this.clearForm());
  };
  clearForm = () =>{
    this.setState({name:'', quantity:'', price:''});
    console.log('cleared');
  };
getSummary(){
  if (this.state.items.length !==0) {
    const summary = [...this.state.items].reduce((sum, curr)=>{
      return sum + curr.quantity * curr.price;
    }, 0);
    return summary;
  } else {
    return 0;
  }
};
remuveItem = (id) =>{
  const items = [...this.state.items];
  items.splice(id,1);
  axios.delete('/items.'+id+'json');
};
componentDidMount(){
  this.setInfoInState();
}
  render() {
    return (
      <div className="App">
        <Form
          valueName={this.state.name}
          valueQuantity={this.state.quantity}
          valuePrice={this.state.price}
          clikedAdd={this.addNewItem}
          changeName={this.getName}
          changeQuantity={this.getQuantity}
          changePrice={this.getPrice}
        />
        <Table
          clikedRemuve={this.remuveItem}
          summary={this.getSummary}
          items={this.state.items}
        />
        <p>Summary: {this.getSummary()}</p>
      </div>
    );
  }
}

export default OrdersWithLoader(Orders, axios);
