import React, {Component, Fragment} from 'react';
import Orders from './Orders';
import ErrorBoundary from '../hoc/ErrorBoundary';

class App extends Component {
  render() {
    return (
      <ErrorBoundary>
        <Orders/>
      </ErrorBoundary>
  );
  }
}

export default App;
