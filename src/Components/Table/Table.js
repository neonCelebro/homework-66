import React from 'react';
import './Table.css';

const Table = props => {
  return (
    <table>
      <colgroup>
        <col span="2" style={{width: '30%'}}/>
        <col span="1" style={{width: '25%'}}/>
      </colgroup>
      <thead>
        <tr>
          <th>Name</th>
          <th>Quantity</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
          {
            props.items.length !== 0? props.items.map((item, id)=>{
              return(
                <tr key={id} className="items">
                  <td>{item.name}</td>
                  <td>{item.quantity}</td>
                  <td>{item.price}</td>
                  <td><button className="remuveItem" onClick={(e)=>props.clikedRemuve(id)}></button></td>
                </tr>
              )
            }) : <tr><td>Information is absent</td></tr>
          }
      </tbody>
    </table>
  )
};

export default Table;
