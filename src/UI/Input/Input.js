import React from 'react';

const Input = props => (
  <input
    value={props.value}
    required={props.required}
    placeholder={props.placeholder}
    type={props.type}
    name={props.name}
    onChange={props.change}/>
);

export default Input;
