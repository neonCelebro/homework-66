import React from 'react';
import Input from '../Input/Input';
import './Form.css';

const Form = props => {
  return (
    <form onSubmit={props.clikedAdd}>
      <Input value={props.valueName} change={(e)=>props.changeName(e)} required placeholder="Name" type="text" name='name' />
      <Input value={props.valueQuantity} change={(e)=>props.changeQuantity(e)} required placeholder="Quantity" type="number" name='Quantity'/>
      <Input value={props.valuePrice} change={(e)=>props.changePrice(e)} required placeholder="Price" type="number" name='Price'/>
      <button className="sendButton" type="submit" name="send" >add item</button>
    </form>
  )
};

export default Form;
