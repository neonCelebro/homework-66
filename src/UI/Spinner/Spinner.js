import React from 'react';
import './Spinner.css';

const Spinner = (props) => (
  <div className={props.hide}>Loading...</div>
);

export default Spinner;
